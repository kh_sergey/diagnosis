﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Diagnosis.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Free = table.Column<bool>(type: "bit", nullable: false),
                    PredeterminedDiagnosis = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GameSessions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Health = table.Column<float>(type: "real", nullable: false),
                    APM = table.Column<float>(type: "real", nullable: false),
                    DialogsCount = table.Column<int>(type: "int", nullable: false),
                    Duration = table.Column<int>(type: "int", nullable: false),
                    k1 = table.Column<float>(type: "real", nullable: false),
                    k2 = table.Column<float>(type: "real", nullable: false),
                    k3 = table.Column<float>(type: "real", nullable: false),
                    userId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameSessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GameSessions_Users_userId",
                        column: x => x.userId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Free", "Name", "PredeterminedDiagnosis" },
                values: new object[] { 1, false, "Пряткин Семён Петрович", "Тяжелое Депрессивное Расстройство (DSM-5)" });

            migrationBuilder.InsertData(
                table: "GameSessions",
                columns: new[] { "Id", "APM", "Date", "DialogsCount", "Duration", "Health", "k1", "k2", "k3", "userId" },
                values: new object[] { 1, 54f, new DateTime(2024, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), 16, 41, 1.5f, 0.5f, 0.6f, 0.8f, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_GameSessions_userId",
                table: "GameSessions",
                column: "userId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameSessions");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
