﻿using Diagnosis.Models;
using Diagnosis.Models.ModelViews;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Diagnosis.Controllers
{
    public class GameSessionController : Controller
    {
        ApiController apiController;

        public GameSessionController(ApplicationContext context)
        {
            apiController = new ApiController(context);   
        }

        public ActionResult Index()
        {
            try
            {
                IActionResult? g = apiController.GetGameSessions();
                var content = g as ContentResult;
                var json = content.Content;
                var data = JsonConvert.DeserializeObject<IEnumerable<GameSession>>(json);

                return View(data);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult GetGameSession(int id)
        {
            try
            {
                IActionResult? g = apiController.GetGameSessionById(id);

                if (g != null)
                {
                    var content = g as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<GameSession>(json);

                    return View("Read", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult UpdateGameSession(int id)
        {
            try
            {
                IActionResult? g = apiController.GetGameSessionById(id);

                if (g != null)
                {
                    var content = g as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<GameSession>(json);

                    return View("Update", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateGameSession(GameSession g)
        {
            try
            {
                GameSession_EditView g_ev = new GameSession_EditView();
                g_ev.Date = g.Date;
                g_ev.Health = g.Health;
                g_ev.APM = g.APM;
                g_ev.DialogsCount = g.DialogsCount;
                g_ev.Duration = g.Duration;
                g_ev.k1 = g.k1;
                g_ev.k2 = g.k2;
                g_ev.k3 = g.k3;
                g_ev.userId = g.userId;

                apiController.EditGameSession(g_ev, g.Id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult DeleteGameSession(int id)
        {
            try
            {
                IActionResult? g = apiController.GetGameSessionById(id);

                if (g != null)
                {
                    var content = g as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<GameSession>(json);

                    return View("Delete", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteGameSession(GameSession g)
        {
            try
            {
                if (g != null)
                {
                    apiController.DeleteGameSession(g.Id);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult CreateGameSession()
        {
            try
            {
                return PartialView("Create");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGameSession(GameSession g)
        {
            try
            {
                GameSession_AddView g_av = new GameSession_AddView();
                g_av.Date = g.Date;
                g_av.Health = g.Health;
                g_av.APM = g.APM;
                g_av.DialogsCount = g.DialogsCount;
                g_av.Duration = g.Duration;
                g_av.k1 = g.k1;
                g_av.k2 = g.k2;
                g_av.k3 = g.k3;
                g_av.userId = g.userId;

                apiController.AddGameSession(g_av);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}
