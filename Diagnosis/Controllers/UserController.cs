﻿using Diagnosis.Models;
using Diagnosis.Models.ModelViews;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Diagnosis.Controllers
{
    public class UserController : Controller
    {
        ApiController apiController;

        public UserController(ApplicationContext context)
        {
            apiController = new ApiController(context);
        }

        public IActionResult Index()
        {
            try
            {
                IActionResult? u = apiController.GetUsers();
                var content = u as ContentResult;
                var json = content.Content;
                var data = JsonConvert.DeserializeObject<IEnumerable<User>>(json);

                return View(data);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult GetUser(int id)
        {
            try
            {
                IActionResult? u = apiController.GetUserById(id);

                if (u != null)
                {
                    var content = u as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<User>(json);

                    return View("Read", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult UpdateUser(int id)
        {
            try
            {
                IActionResult? u = apiController.GetUserById(id);

                if (u != null)
                {
                    var content = u as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<User>(json);

                    return View("Update", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUser(User u)
        {
            try
            {
                User_EditView u_ev = new User_EditView();
                u_ev.Name = u.Name;
                u_ev.Free = u.Free;
                u_ev.PredeterminedDiagnosis = u.PredeterminedDiagnosis;

                apiController.EditUser(u_ev, u.Id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult DeleteUser(int id)
        {
            try
            {
                IActionResult? u = apiController.GetUserById(id);

                if (u != null)
                {
                    var content = u as ContentResult;
                    var json = content.Content;
                    var data = JsonConvert.DeserializeObject<User>(json);

                    return View("Delete", data);
                }

                return View("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUser(User u)
        {
            try
            {
                if (u != null)
                {
                    apiController.DeleteUser(u.Id);
                }

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult CreateUser()
        {
            try
            {
                return PartialView("Create");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser(User u)
        {
            try
            {
                User_AddView u_av = new User_AddView();
                u_av.Name = u.Name;
                u_av.Free = u.Free;
                u_av.PredeterminedDiagnosis = u.PredeterminedDiagnosis;

                apiController.AddUser(u_av);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}
