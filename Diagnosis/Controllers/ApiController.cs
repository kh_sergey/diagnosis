﻿using Diagnosis.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Diagnosis.Models.ModelViews;

namespace Diagnosis.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        ApplicationContext db;

        public ApiController(ApplicationContext db)
        {
            this.db = db;
        }

        #region GameSession

        /// <summary>
        /// Получить записи игровых сессий
        /// </summary>
        [HttpGet("GameSessions")]
        public IActionResult GetGameSessions()
        {
            try
            {
                dynamic g = new JArray();

                var g_l = db.GameSessions?.ToList();

                foreach (GameSession g_g in g_l)
                {
                    g.Add(new JObject(
                        new JProperty("Id", g_g.Id),
                        new JProperty("Date", g_g.Date),
                        new JProperty("Health", g_g.Health),
                        new JProperty("APM", g_g.APM),
                        new JProperty("DialogsCount", g_g.DialogsCount),
                        new JProperty("Duration", g_g.Duration),
                        new JProperty("k1", g_g.k1),
                        new JProperty("k2", g_g.k2),
                        new JProperty("k3", g_g.k3),
                        new JProperty("UserId", g_g.userId)
                        ));
                }
                string respStr = g.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Получить запись игровой сессии по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        [HttpGet("GameSession/{id}")]
        public IActionResult GetGameSessionById(int id)
        {
            try
            {
                GameSession? gameSession = db.GameSessions?.FirstOrDefault(g => g.Id == id);

                if (gameSession != null)
                {
                    gameSession = db.GameSessions?.FirstOrDefault(g => g.Id == id);
                    gameSession.user = db.Users.FirstOrDefault(u => u.Id == gameSession.userId);

                    string respStr = JsonConvert.SerializeObject(gameSession, Formatting.Indented);

                    return new ContentResult() { Content = respStr, StatusCode = 200 };
                }
                {
                    return new ContentResult() { Content = "FAIL", StatusCode = 404 };
                }
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Добавить запись игровой сессии
        /// </summary>
        /// <remarks>
        /// Пример:
        /// 
        ///     POST api/GameSession
        ///     {        
        ///       "Date": "2024-05-12",
        ///       "Health": 5,
        ///       "APM": 48.4454,
        ///       "DialogsCount": 13,
        ///       "Duration": 32,
        ///       "k1": 0.6,
        ///       "k2": 0.3,
        ///       "k3": 0.2,
        ///       "userid": 1,
        ///     }
        /// </remarks>
        /// <param name="data">Данные новой записи запроса</param>
        [HttpPost("GameSession")]
        public IActionResult AddGameSession([FromBody] GameSession_AddView data)
        {
            try
            {
                dynamic resp = new JObject();

                DateTime Date = data.Date;
                float Health = data.Health;
                float APM = data.APM;
                int DialogsCount = data.DialogsCount;
                int Duration = data.Duration;
                float k1 = data.k1;
                float k2 = data.k2;
                float k3 = data.k3;
                int userid = data.userId;

                GameSession gameSession = new GameSession();

                gameSession.Date = Date;
                gameSession.Health = Health;
                gameSession.APM = APM;
                gameSession.DialogsCount = DialogsCount;
                gameSession.Duration = Duration;
                gameSession.k1 = k1;
                gameSession.k2 = k2;
                gameSession.k3 = k3;
                gameSession.userId = userid;

                gameSession.user = db.Users.FirstOrDefault(u => u.Id == gameSession.userId);

                db.GameSessions.Add(gameSession);
                db.SaveChanges();
                resp = JsonConvert.SerializeObject(gameSession, Formatting.Indented);

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Изменить запись игровой сессии
        /// </summary>
        /// <remarks>
        /// Пример:
        /// 
        ///     PUT api/GameSession
        ///     {        
        ///       "Date": "2024-05-12",
        ///       "Health": 5,
        ///       "APM": 48.4454,
        ///       "DialogsCount": 13,
        ///       "Duration": 32,
        ///       "k1": 0.6,
        ///       "k2": 0.3,
        ///       "k3": 0.2,
        ///       "userid": 1,
        ///     }
        /// </remarks>
        /// <param name="data">Данные для изменения</param>
        /// <param name="id">Идентификатор изменяемой записи</param>
        [HttpPut("GameSession/{id}")]
        public IActionResult EditGameSession([FromBody] GameSession_EditView data, int id)
        {
            try
            {
                dynamic resp = new JObject();

                DateTime Date = data.Date;
                float Health = data.Health;
                float APM = data.APM;
                int DialogsCount = data.DialogsCount;
                int Duration = data.Duration;
                float k1 = data.k1;
                float k2 = data.k2;
                float k3 = data.k3;
                int userid = data.userId;

                GameSession gameSession = db.GameSessions.FirstOrDefault(g => g.Id == id);

                gameSession.Date = Date;
                gameSession.Health = Health;
                gameSession.APM = APM;
                gameSession.DialogsCount = DialogsCount;
                gameSession.Duration = Duration;
                gameSession.k1 = k1;
                gameSession.k2 = k2;
                gameSession.k3 = k3;
                gameSession.userId = userid;

                gameSession.user = db.Users.FirstOrDefault(u => u.Id == gameSession.userId);

                db.GameSessions.Update(gameSession);
                db.SaveChanges();
                resp = JsonConvert.SerializeObject(gameSession, Formatting.Indented);

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Удалить запись игровой сессии по идентификатору записи
        /// </summary>
        /// <param name="id">Идентификатор записи для удаления</param>
        [HttpDelete("GameSession/{id}")]
        public IActionResult DeleteGameSession(int id)
        {
            try
            {
                dynamic resp = new JObject();
                GameSession g = db.GameSessions.FirstOrDefault(g => g.Id == id);

                db.GameSessions.Remove(g);
                db.SaveChanges();
                resp.id = g.Id;
                resp.status = "DELETED";

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        #endregion

        #region User

        /// <summary>
        /// Получить записи пользователей
        /// </summary>
        [HttpGet("Users")]
        public IActionResult GetUsers()
        {
            try
            {
                dynamic u = new JArray();

                var u_l = db.Users?.ToList();

                foreach (User u_u in u_l)
                {
                    u.Add(new JObject(
                        new JProperty("Id", u_u.Id),
                        new JProperty("Name", u_u.Name),
                        new JProperty("Free", u_u.Free),
                        new JProperty("PredeterminedDiagnosis", u_u.PredeterminedDiagnosis)
                        ));
                }
                string respStr = u.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Получить запись пользователя по ее идентификатору
        /// </summary>
        /// <param name="id">Идентификатор записи</param>
        [HttpGet("User/{id}")]
        public IActionResult GetUserById(int id)
        {
            try
            {
                User? user = db.Users?.FirstOrDefault(u => u.Id == id);

                if (user != null)
                {

                    user = db.Users?.FirstOrDefault(u => u.Id == id);

                    string respStr = JsonConvert.SerializeObject(user, Formatting.Indented);

                    return new ContentResult() { Content = respStr, StatusCode = 200 };
                }
                {
                    return new ContentResult() { Content = "Not Found", StatusCode = 404 };
                }
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Добавить запись пользователя
        /// </summary>
        /// <remarks>
        /// Пример:
        /// 
        ///     POST api/User
        ///     {        
        ///       "Name": "Пряткин Семён Петрович",
        ///       "Free": false,
        ///       "PredeterminedDiagnosis": "Тяжелое Депрессивное Расстройство (DSM-5)"
        ///     }
        /// </remarks>
        /// <param name="data">Данные новой записи</param>
        [HttpPost("User")]
        public IActionResult AddUser([FromBody] User_AddView data)
        {
            try
            {
                dynamic resp = new JObject();

                string Name = data.Name;
                bool Free = data.Free;
                string PredeterminedDiagnosis = data.PredeterminedDiagnosis;

                User user = new User();

                user.Name = Name;
                user.Free = Free;
                user.PredeterminedDiagnosis = PredeterminedDiagnosis;

                db.Users.Add(user);
                db.SaveChanges();
                resp = JsonConvert.SerializeObject(user, Formatting.Indented);

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 201 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Изменить запись пользователя
        /// </summary>
        /// <remarks>
        /// Пример:
        /// 
        ///     PUT api/User
        ///     {        
        ///       "Name": "Пряткин Семён Петрович",
        ///       "Free": false,
        ///       "PredeterminedDiagnosis": "Тяжелое Депрессивное Расстройство (DSM-5)"
        ///     }
        /// </remarks>
        /// <param name="data">Данные для изменения</param>
        /// <param name="id">Идентификатор изменяемой записи</param>
        [HttpPut("User/{id}")]
        public IActionResult EditUser([FromBody] User_EditView data, int id)
        {
            try
            {
                dynamic resp = new JObject();

                string Name = data.Name;
                bool Free = data.Free;
                string PredeterminedDiagnosis = data.PredeterminedDiagnosis;

                User user = db.Users.FirstOrDefault(u => u.Id == id);

                user.Name = Name;
                user.Free = Free;
                user.PredeterminedDiagnosis = PredeterminedDiagnosis;

                db.Users.Update(user);
                db.SaveChanges();
                resp = JsonConvert.SerializeObject(user, Formatting.Indented);

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        /// <summary>
        /// Удалить запись пользователя по идентификатору записи
        /// </summary>
        /// <param name="id">Идентификатор записи для удаления</param>
        [HttpDelete("User/{id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                dynamic resp = new JObject();
                User u = db.Users.FirstOrDefault(u => u.Id == id);

                db.Users.Remove(u);
                db.SaveChanges();
                resp.id = u.Id;
                resp.status = "DELETED";

                string respStr = resp.ToString();

                return new ContentResult() { Content = respStr, StatusCode = 200 };
            }
            catch (Exception ex)
            {
                return new ContentResult() { Content = ex.Message, StatusCode = 400 };
            }
        }

        #endregion
    }
}
