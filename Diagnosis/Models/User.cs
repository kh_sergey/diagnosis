﻿namespace Diagnosis.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Free { get; set; }
        public string? PredeterminedDiagnosis { get; set; }
    }
}
