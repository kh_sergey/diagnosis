﻿namespace Diagnosis.Models.ModelViews
{
    public class User_AddView
    {
        public string Name { get; set; }
        public bool Free { get; set; }
        public string? PredeterminedDiagnosis { get; set; }
    }
}
