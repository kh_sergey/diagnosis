﻿namespace Diagnosis.Models.ModelViews
{
    public class GameSession_EditView
    {
        public DateTime Date { get; set; }
        public float Health { get; set; }
        public float APM { get; set; }
        public int DialogsCount { get; set; }
        public int Duration { get; set; }
        public float k1 { get; set; }
        public float k2 { get; set; }
        public float k3 { get; set; }
        public int userId { get; set; }
    }
}
