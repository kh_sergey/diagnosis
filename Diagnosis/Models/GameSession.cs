﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Diagnosis.Models
{
    public class GameSession
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public float Health { get; set; }
        public float APM { get; set; }
        public int DialogsCount { get; set; }
        public int Duration { get; set; }
        public float k1 { get; set; }
        public float k2 { get; set; }
        public float k3 { get; set; }

        [ForeignKey("User")]
        public int userId { get; set; }
        public User user { get; set; }
    }
}
