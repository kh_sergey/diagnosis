﻿using Microsoft.EntityFrameworkCore;

namespace Diagnosis.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<GameSession> GameSessions { get; set; }
        public DbSet<User> Users { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options = null) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            GameSession gameSession1 = new GameSession
            {
                Id = 1,
                Date = new DateTime(2024, 05, 12),
                Health = 1.5f,
                APM = 54f,
                DialogsCount = 16,
                Duration = 41,
                k1 = 0.5f,
                k2 = 0.6f,
                k3 = 0.8f,

                userId = 1,
            };
            User user1 = new User
            {
                Id = 1,
                Name = "Пряткин Семён Петрович",
                Free = false,
                PredeterminedDiagnosis = "Тяжелое Депрессивное Расстройство (DSM-5)"
            };

            modelBuilder.Entity<User>().HasData(user1);
            modelBuilder.Entity<GameSession>().HasData(gameSession1);
        }
    }
}
