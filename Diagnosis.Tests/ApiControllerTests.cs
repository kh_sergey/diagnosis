using Diagnosis.Controllers;
using Diagnosis.Models;
using Diagnosis.Models.ModelViews;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Diagnosis.Tests
{
    public class ApiControllerTests
    {
        private readonly IServiceProvider _serviceProvider;

        public ApiControllerTests()
        {
            _serviceProvider = DependencyInjection.InitilizeServices().BuildServiceProvider();
        }

        #region GameSession

        [Fact]
        public void GetGameSessionsSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            ContentResult result = controller.GetGameSessions() as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void GetGameSessionByIdSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            GameSession gameSession = new GameSession
            {
                Id = 1,
                Date = new DateTime(2024, 05, 12),
                Health = 5f,
                APM = 48.4454f,
                DialogsCount = 13,
                Duration = 32,
                k1 = 0.6f,
                k2 = 0.3f,
                k3 = 0.2f,

                userId = 1,
            };

            // Act
            ContentResult result = controller.GetGameSessionById(1) as ContentResult;
            var rr = JsonConvert.DeserializeObject<GameSession>(result.Content);

            bool eq =
            (
                gameSession.Id == rr.Id &&
                gameSession.Date == rr.Date &&
                gameSession.Health == rr.Health &&
                gameSession.APM == rr.APM &&
                gameSession.DialogsCount == rr.DialogsCount &&
                gameSession.Duration == rr.Duration &&
                gameSession.k1 == rr.k1 &&
                gameSession.k2 == rr.k2 &&
                gameSession.k3 == rr.k3 &&
                gameSession.userId == rr.userId
            );

            // Assert
            Assert.NotNull(result);
            Assert.Equal(eq, true);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void AddGameSessionSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            GameSession_AddView newitem = new GameSession_AddView();
            newitem.k1 = 0.6f;
            newitem.k2 = 0.3f;
            newitem.k3 = 0.2f;
            newitem.Duration = 32;
            newitem.DialogsCount = 13;
            newitem.APM = 48.4454f;
            newitem.Health = 5;
            newitem.Date = new DateTime(2024, 05, 12);
            newitem.userId = 1;

            ContentResult result = controller.AddGameSession(newitem) as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 201);
        }

        [Fact]
        public void EditGameSessionSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            GameSession_EditView newitem = new GameSession_EditView();
            newitem.k1 = 0.6f;
            newitem.k2 = 0.3f;
            newitem.k3 = 0.2f;
            newitem.Duration = 32;
            newitem.DialogsCount = 13;
            newitem.APM = 48.4454f;
            newitem.Health = 5;
            newitem.Date = new DateTime(2024, 05, 12);
            newitem.userId = 1;

            ContentResult result = controller.EditGameSession(newitem, 1) as ContentResult;

            var rr = JsonConvert.DeserializeObject<GameSession>(result.Content);

            bool eq =
            (
            newitem.k1 == rr.k1 &&
            newitem.k2 == rr.k2 &&
            newitem.k3 == rr.k3 &&
            newitem.Duration == rr.Duration &&
            newitem.DialogsCount == rr.DialogsCount &&
            newitem.APM == rr.APM &&
            newitem.Health == rr.Health &&
            newitem.Date == rr.Date &&
            newitem.userId == rr.userId
            );
            // Assert
            Assert.NotNull(result);
            Assert.Equal(eq, true);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void DeleteGameSessionSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            ContentResult result = controller.DeleteGameSession(1) as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }
        #endregion

        #region User

        [Fact]
        public void GetUsersSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            ContentResult result = controller.GetUsers() as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void GetUserByIdSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            User testrr = new User
            {
                Id = 1,
                Name = "������� ���� ��������",
                Free = false,
                PredeterminedDiagnosis = "������� ������������ ������������ (DSM-5)"
            };

            // Act
            ContentResult result = controller.GetUserById(1) as ContentResult;
            var rr = JsonConvert.DeserializeObject<User>(result.Content);

            bool eq =
            (
            testrr.Id == rr.Id &&
            testrr.Name == rr.Name &&
            testrr.Free == rr.Free &&
            testrr.PredeterminedDiagnosis == rr.PredeterminedDiagnosis
            );

            // Assert
            Assert.NotNull(result);
            Assert.Equal(eq, true);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void AddUserSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            User_AddView newitem = new User_AddView();
            newitem.Name = "������� ���� ��������";
            newitem.Free = false;
            newitem.PredeterminedDiagnosis = "������� ������������ ������������ (DSM-5)";

            ContentResult result = controller.AddUser(newitem) as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 201);
        }
        [Fact]
        public void EditUserSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            User_EditView newitem = new User_EditView();
            newitem.Name = "������� ���� ��������";
            newitem.Free = false;
            newitem.PredeterminedDiagnosis = "������� ������������ ������������ (DSM-5)";

            ContentResult result = controller.EditUser(newitem, 1) as ContentResult;

            var rr = JsonConvert.DeserializeObject<User>(result.Content);

            bool eq =
            (
            newitem.Name == rr.Name &&
            newitem.Free == rr.Free &&
            newitem.PredeterminedDiagnosis == rr.PredeterminedDiagnosis
            );

            // Assert
            Assert.NotNull(result);
            Assert.Equal(eq, true);
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public void DeleteUserSuccessTest()
        {
            var db = _serviceProvider.GetRequiredService<ApplicationContext>();
            // Arrange
            ApiController controller = new ApiController(db);
            // Act
            ContentResult result = controller.DeleteUser(1) as ContentResult;
            // Assert
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 200);
        }
        #endregion
    }
}